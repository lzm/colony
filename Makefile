SRC=src/*.ls
LSC=lsc
BUNDLE=browserify -x prelude-ls

all:
	$(LSC) -c $(SRC)
	$(BUNDLE) src/main.js -o www/main.js

watch:
	pywatch "make" src/*.ls
