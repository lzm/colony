{filter, tail, last, any} = require 'prelude-ls'
simplex-noise = require 'simplex-noise'
a-star = require 'a-star'

canvas = null
ctx = null

resize-canvas = !->
    canvas.width = window.inner-width
    canvas.height = window.inner-height

init-canvas = !->
    canvas := document.get-element-by-id 'canvas'
    throw 'canvas elem not found' unless canvas

    ctx := canvas.get-context '2d'
    throw 'could not get canvas 2d context' unless ctx

    window.add-event-listener 'resize', resize-canvas, false
    window.add-event-listener 'mousemove', mouse-move, false
    window.add-event-listener 'mousedown', mouse-down, false
    resize-canvas!

    set-interval tick, 100

mouse-x = -1
mouse-y = -1

mouse-move = (e) !->
    mouse-x := parse-int(e.x / tile-size)
    mouse-y := parse-int(e.y / tile-size)

tile-size = 16
half-size = tile-size / 2
map-height = 40
map-width = 80
grass-noise = new simplex-noise!
mountain-noise = new simplex-noise!

random-tile = (x, y) ->
    mountain-scale = 0.05
    value = mountain-noise.noise2D x * mountain-scale, y * mountain-scale
    y-normalized = ((y / map-height) - 0.5) * 4 - 0.5
    if value < y-normalized
        return {type: 'mountain', passable: false}

    grass-scale = 0.1
    value = grass-noise.noise2D x * grass-scale, y * grass-scale
    if value < 0.3
        return {type: 'ground', passable: true}
    else
        return {type: 'grass', passable: true}

game-state =
    tiles: [[random-tile x, y for x til map-width] for y til map-height]
    entities: [{
        type: 'colonist'
        x: map-width / 2
        y: map-height / 4
        walk-delay: 1
        wait: null
        path: []
        job: null
    }]
    jobs: []

tile-style =
    ground: '#2F4F2F'
    grass: '#238E23'
    mountain: '#5C3317'
    cave: '#C3834C'

draw-tile = (x, y, tile) !->
    ctx.save!
    ctx.translate x * tile-size, y * tile-size

    ctx.fill-style = tile-style[tile.type]
    #ctx.line-width = 0

    ctx.begin-path!
    ctx.move-to 0, 0
    ctx.line-to 0, tile-size
    ctx.line-to tile-size, tile-size
    ctx.line-to tile-size, 0
    ctx.line-to 0, 0

    ctx.fill!

    if tile.clicked
        ctx.stroke-style = 'white'
        ctx.stroke!

    if x == mouse-x and y == mouse-y
        ctx.stroke-style = 'black'
        ctx.stroke!

    ctx.restore!

draw-entity = (entity) !->
    ctx.save!
    ctx.translate entity.x * tile-size, entity.y * tile-size

    ctx.fill-style = 'white'
    ctx.stroke-style = 'black'

    ctx.begin-path!
    ctx.arc half-size, half-size, half-size, 0, 2 * Math.PI, false

    ctx.fill!
    ctx.stroke!

    ctx.restore!

draw-path = (x, y, path) !->
    if !path or path.length == 0
        return

    ctx.save!

    ctx.stroke-style = 'gray'
    ctx.line-width = 1

    ctx.begin-path!
    ctx.move-to x * tile-size + half-size, y * tile-size + half-size
    for [x, y] in path
        ctx.line-to x * tile-size + half-size, y * tile-size + half-size

    ctx.stroke!

    ctx.restore!

draw-tiles = (tiles) !->
    for row, y in tiles
        for tile, x in row
            draw-tile x, y, tile

sign = (n) -> if n < 0 then -1 else if n > 0 then 1 else 0

entity-tick = (entity) !->
    if !entity.job
        for job in game-state.jobs
            path = find-path-neighbor [entity.x, entity.y], [job.x, job.y]
            if path != null
                game-state.jobs = filter (!= job), game-state.jobs
                entity.path = path
                entity.job = job
                break

    if entity.path and entity.path.length > 0
        entity-move entity, entity.path[0]
        return

    if entity.job
        if entity.wait == null
            entity.wait = entity.job.delay

        if entity.wait == 0
            entity-work entity, entity.job
            entity.wait = null
            entity.job = null
            return

        entity.wait -= 1

entity-work = (entity, job) !->
    if job.type == 'dig'
        game-state.tiles[job.y][job.x] = {type:'cave', passable: true}

entity-move = (entity, [x, y]) !->
    if entity.wait == null
        entity.wait = entity.walk-delay

    if entity.wait == 0
        entity.x = x
        entity.y = y
        entity.path = tail entity.path
        console.log entity.path
        entity.wait = null
        return

    entity.wait -= 1

tick = !->
    for entity in game-state.entities
        entity-tick entity

    draw-tiles game-state.tiles

    for entity in game-state.entities
        draw-path entity.x, entity.y, entity.path

    for entity in game-state.entities
        draw-entity entity

in-bounds = ([x, y]) -> x >= 0 and x < map-width and y >= 0 and y < map-height
passable = ([x, y]) -> game-state.tiles[y][x].passable

neighbors = ([x, y]) -> filter (-> in-bounds it and passable it), [
    [x - 1, y - 1] [x, y - 1] [x + 1, y - 1]
    [x - 1, y    ]            [x + 1, y    ]
    [x - 1, y + 1] [x, y + 1] [x + 1, y + 1]]

distance = ([x1, y1], [x2, y2]) --> Math.sqrt((y1 - y2) ** 2 + (x1 - x2) ** 2)

eq = (a, b) --> (a >= b and a <= b)

find-path = (src, dst) ->
    result = a-star do
        start: src
        is-end: eq dst
        neighbor: neighbors
        distance: -> 1
        heuristic: distance dst

    if result.status == 'success'
        result.path
    else
        null

find-path-neighbor = (src, dst) ->
    dst-neighbors = neighbors dst

    result = a-star do
        start: src
        is-end: -> any (eq it), dst-neighbors
        neighbor: neighbors
        distance: -> 1
        heuristic: distance dst

    if result.status == 'success'
        tail result.path
    else
        null

tile-click = (x, y, tile) ->
    if tile.type != 'mountain'
        return

    if tile.clicked
        return

    tile.clicked = true

    game-state.jobs.push {type: 'dig', x, y, delay: 10}

mouse-down = (e) !->
    mouse-move e
    if in-bounds [mouse-x, mouse-y]
        tile-click mouse-x, mouse-y, game-state.tiles[mouse-y][mouse-x]

init-canvas!
